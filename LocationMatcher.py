# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 09:41:55 2022

@author: JacobHarden
"""
#initial location matching for majority of counties to historic counties

def locationmatcher(radiationfile, locationfile, outputfile):

    import numpy as np
    import pandas as pd 

    radiationdata = pd.read_csv(radiationfile)
    locationdata = pd.read_csv(locationfile, on_bad_lines='skip', usecols = ['county'])
    locationdata.drop_duplicates(inplace=True)
    counties=locationdata['county'].tolist()
    radiationdatas = pd.DataFrame()
    for i in counties:
        radiationdatas[i] = radiationdata['County'].str.contains(i, case=False)
        
    radiationdatas = radiationdatas.dot(radiationdatas.columns)

    radiationdata['matchedcounty'] = radiationdatas
    radiationdata['matchedcounty'].loc[radiationdata['Location'] == 'London'] = "Middlesex"

    out = radiationdata.to_csv(outputfile)
    return out
