# -*- coding: utf-8 -*-
"""
Created on Fri Apr  8 10:04:25 2022

@author: JacobHarden
"""


def radiationdata(targetfolder, outfile):
    import os
    import pandas as pd
    x = pd.concat([pd.read_csv(os.path.join(targetfolder,file), encoding='windows-1252') for file in os.listdir(targetfolder)])
    x.drop(columns = ['Site Normal Level', 'Std Dev', 'Standard Deviation', 'Std Deviation'], inplace=True)
    x = x[x['Admin Division 2 (County/district/other)'] != '#FIELD!']
    x.rename(columns = {'Admin Division 2 (County/district/other)' : 'County'}, inplace=True)
    return x.to_csv(os.path.join(targetfolder,outfile))



