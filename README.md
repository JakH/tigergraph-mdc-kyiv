# TigerGraph MDC - Kyiv

Tigergraph MDC submission code repository

## Environmental impacts on lung disease and cancer related hospital admissions

Contributors:
- Jacob Harden
- Georgina Bridgman
- Francesca Froughi
- Matthew Watling

It has been a point of contention as to whether or not modern Britain still suffers significantly from air pollution related illnesses after the strong push to clean up the country's air in recent years. This project aims to create an easily navigable database from which to analyse the links between air pollutants, ambient background radiation (unlikely but always worth a look) and lung disease/cancer related hospital admissions across counties in the UK to provide local governments with easy assessments on the potential risks of certain pollutants.

There are no specific dependencies as external software was mostly used for data cleansing and scraping. The tigergraph solution itself can be found as a gzip file in the repo.

## Getting Started

Within this repo are the raw data files for Lung, cancer, radiation and air quality. There are multiple python scripts that can be used to clean the data and load into the desired format for TigerGraph.

## Known Issues

Unfortunately, GDPR greatly limited the granularity of the dataset with only a yearly count of cases available. This presented challenges when working within a graph database system, future work should aim to retrieve daily data and more localised pollution data in order to more thoroughly explore links.

## Reflections and future improvements

Further potential next steps could include other factors such as smoking and general population health data to further determine the causation vs correlation question when it comes to the links between air pollution and hospital admissions. A timed offset for admissions after a certain period of high radiation/pollution would also help catch more long term health effects which would not neccessarily present immiediately. Ultimately, working around GDPR limits the detail and graphability of the project data which is not easily remedied without specialist access to sensitive information.


